﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace easysql
{
    public class SqliteDatabase:BaseDatabase
    {
        private String _connString;
        public SqliteDatabase(String connString)
            : base("@Param", "@Param")
        {
            this._connString = connString;
        }

        #region 必须重载的父类方法

        protected override System.Data.Common.DbConnection CreateConnection()
        {
            return new SQLiteConnection(_connString);
        }

        protected override System.Data.Common.DbDataAdapter CreateAdapter(System.Data.Common.DbCommand cmd)
        {
            return new SQLiteDataAdapter(cmd as SQLiteCommand);
        }
        protected override string AutoIncreSql()
        {
            return "select last_insert_rowid() ";
        }

        #endregion 必须重载的父类方法
    }
}
