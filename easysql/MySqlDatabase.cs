﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace easysql
{
    public class MySqlDatabase:BaseDatabase
    {

        private String _connString;

        /// <summary>
        /// 通过连接字符串获得连接
        /// </summary>
        /// <param name="connString">server=数据库地址;database=数据库名;Persist Security Info=False;uid=用户名;pwd=密码</param>
        public MySqlDatabase(String connString)
            : base("@Param", "?@Param")
        {
            this._connString = connString;
        }
        public MySqlDatabase(ConnPool connPool) : base("@Param", "?@Param")
        {
            this._connPool = connPool;
        }

        #region 必须重载的方法
        protected override DbConnection CreateConnection()
        {
            return new MySqlConnection(_connString);
        }
        protected override System.Data.Common.DbDataAdapter CreateAdapter(System.Data.Common.DbCommand cmd)
        {
            return new MySqlDataAdapter(cmd as MySqlCommand);
        }
        protected override string AutoIncreSql()
        {
            return " select last_insert_id()";
        }
        #endregion 必须重载的方法
        protected override string getLimitString(string sql, ref int start, int maxResult)
        {
            sql = sql + " limit " + start + "," + maxResult + " ";
            start = 0;
            return sql;
        }

     
    }
}
