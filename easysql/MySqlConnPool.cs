﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace easysql
{
    public class MySqlConnPool : ConnPool
    {
       
        public MySqlConnPool(String connString):base(connString)
        {

        }
        public MySqlConnPool(String connString, int minIdleConnections, int maxIdleConnections, int maxActiveConnections):base(connString,minIdleConnections,maxIdleConnections,maxActiveConnections)
        {

        }

        protected override DbConnection createConn(String connString)
        {
            return new MySqlConnection(connString);
        }
    }
}
