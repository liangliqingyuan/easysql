﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace easysql
{
    public class MySqlHelper:BaseDBHelper
    {
        private String _connString;
        private ConnPool _connPool;
        public MySqlHelper(String connString)
        {
            _connString = connString;
        }
        public MySqlHelper(ConnPool connPool)
        {
            this._connPool = connPool;
        }

        protected override BaseDatabase CreateDatabase()
        {
            if (this._connPool != null)
            {
                return DatabaseFactory.CreateMySqlDatabase(_connPool);
            }
            return DatabaseFactory.CreateMySqlDatabase(_connString);
        }

    }
}
